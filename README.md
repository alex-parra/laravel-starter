# My Laravel


## Clone this Repo
```
git clone https://github.com/alex-parra/laravel-starter.git APPFOLDER
cd APPFOLDER/_src/
composer install
cp .env.example .env && php artisan key:generate
```
To include the HTML helper (https://laravelcollective.com/docs/5.2/html)
```
composer require laravelcollective/html
composer update
```
Next add to ./config/app.php
```
'providers' => [
    // ...
    Collective\Html\HtmlServiceProvider::class,
    // ...
  ],

'aliases' => [
    // ...
      'Form' => Collective\Html\FormFacade::class,
      'Html' => Collective\Html\HtmlFacade::class,
    // ...
  ],
```



## Create Controller in ./app/Http/Controllers/
```
php artisan make:controller NameOfController
```

## Create Model in ./app/
```
php artisan make:model NameOfModel
```

## Init Authentication ›› ONLY DO THIS ON FRESH INSTALLS
```
php artisan make:auth
```


## Create Migration
```
php artisan make:migration migration_name --create="table_name"
```

## Run Migrations
```
php artisan migrate
```

## Run Seeders
```
php artisan db:seed
# or
php artisan migrate:refresh --seed
```




## Tutorials
- https://www.flynsarmy.com/2015/02/creating-a-basic-todo-application-in-laravel-5-part-1
- https://www.flynsarmy.com/2015/02/install-illuminatehtml-laravel-5/



## Install Manually
```
mkdir APPFOLDER && cd APPFOLDER
composer create-project --prefer-dist laravel/laravel _src
cp -Rp ./_src/public/. . && rm -rf ./_src/public/ #move contents of public folder to root
››› Open ./index.php and replace "../bootstrap/" with "_src/bootstrap"
››› Open ./.htaccess and append:
	# Deny access to private stuff
	RedirectMatch 404 ^/.*/(_src|SOMEFOLDER|ANOTHERFOLDER|(.*)\.scss|\.git|\.(.*)).*$
››› Empty robots.txt
```
- INSTALL COMPLETE